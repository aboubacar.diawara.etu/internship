"
This app display a pharo class doc in html format.
"
Class {
	#name : #WADoc,
	#superclass : #WAComponent,
	#instVars : [
		'aClass'
	],
	#category : #WAPharodoc
}

{ #category : #testing }
WADoc class >> canBeRoot [ 

	^ true.
]

{ #category : #initialization }
WADoc >> class: class [ 
	aClass := class .
]

{ #category : #initialization }
WADoc >> classDescription: html [
	html heading: self title.
	html paragraph: super class comment.
]

{ #category : #initialization }
WADoc >> comments [
	
	^ aClass comment.
]

{ #category : #initialization }
WADoc >> fieldSummary: html [

	html div
			class: 'field-summary';
			with: [
				html heading level3 
					with: 'This section contains fields description'.
				self class instanceVariables do: [ :var |
						html paragraph: var.
				] 
	].
]

{ #category : #initialization }
WADoc >> initForm: html [
	html heading: self sayHello .
	html form: [ 
			html text: 'Class '.
			html textInput
				callback: [ :value | self class: (value asClass)].
     		html submitButton
				callback: [ self inform: self comments].
		 ]
]

{ #category : #initialization }
WADoc >> initialize [

	super initialize.
	aClass := self class.
]

{ #category : #initialization }
WADoc >> methodSummary: html [
		html div
			class: 'method-summary';
			with: [
				html heading level3 
					with: 'This section contains method description'.
					self class methods do: [ :var |
						html paragraph: var.
					] 
	].
]

{ #category : #initialization }
WADoc >> renderContentOn: html [
	
	self initForm: html.
]

{ #category : #initialization }
WADoc >> sayHello [

	^ 'Hello, i am a class comments displayer, nice to be used by you :)'.
]

{ #category : #initialization }
WADoc >> title [
	
	^ 'Class ', aClass name.
]
